package kienmt.app.edm.com.musicedm;

import android.app.Application;

public class AppApplication extends Application {
    private static AppApplication mSelf;

    public static AppApplication self() {
        return mSelf;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSelf = this;
    }
}
