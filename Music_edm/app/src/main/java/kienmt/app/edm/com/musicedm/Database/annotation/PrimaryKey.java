package kienmt.app.edm.com.musicedm.Database.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PrimaryKey {
	boolean autoIncrease() default true;
}

